var express = require('express');
var router = express.Router();
const neo4j = require('neo4j-driver')


const uri = 'bolt://localhost';
const user = 'neo4j'; 
const password = 'neo4j';

const driver = neo4j.driver(uri, neo4j.auth.basic(user, password))

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});


/* GET home page. */
router.get('/ring', function(req, res, next) {
  const session = driver.session()
  session.run(
    `
    MATCH 		(accountHolder:AccountHolder)-[]->(contactInformation)
    WITH 		contactInformation,
          count(accountHolder) AS RingSize
    MATCH 		(contactInformation)<-[]-(accountHolder),
          (accountHolder)-[r:HAS_CREDITCARD|HAS_UNSECUREDLOAN]->(unsecuredAccount)
    WITH 		collect(DISTINCT accountHolder.UniqueId) AS AccountHolders,
          contactInformation, RingSize,
          SUM(CASE type(r)
            WHEN 'HAS_CREDITCARD' THEN unsecuredAccount.Limit
            WHEN 'HAS_UNSECUREDLOAN' THEN unsecuredAccount.Balance
            ELSE 0
          END) as FinancialRisk
    WHERE 		RingSize > 1
    RETURN 		AccountHolders AS FraudRing,
          labels(contactInformation) AS ContactType,
          RingSize,
          round(FinancialRisk) as FinancialRisk
    ORDER BY 	FinancialRisk DESC
    `
  )
  .then(result => {
    // console.log(result)
    let lst = []; 
    result.records.forEach(record => {
      let temp = {};
      temp['FraudRing'] = record.get('FraudRing');
      temp['ContactType'] = record.get('ContactType');
      temp['RingSize'] = record.get('RingSize').low; 
      temp['FinancialRisk'] = record.get('FinancialRisk');
      lst.push(temp);
    })

    res.send(lst);
  })
  .catch(error => {
    res.send(null,500);
    throw error;
  })
  .finally(() => {
    // return session.close();
  });
});

router.get('/fraud', function(req, res, next) {
  const session = driver.session()
  session.run(
    `MATCH (n1)-[r1]->(n2)
    RETURN n1,n2,r1
    `
  )
  .then(result => {
    // return result.records.map(record => {
    //   console.log(JSON.stringify(record));
    //   // console.log(record.get(0))
    // });
          // console.log(JSON.stringify(result.records));
    
            let nodeSet = new Set(); 
            let nodeList = [];
            let linkList = [];

            result.records.forEach(
            r => 
                {
                  temp1 = {}; 
                  temp1["id"] = r.get('n1').identity.low;
                  temp1["labels"] =  r.get('n1').labels;
                  temp1["properties"] = r.get('n1').properties; 

                  temp2 = {}; 
                  temp2["id"] = r.get('n2').identity.low;
                  temp2["labels"] =  r.get('n2').labels;
                  temp2["properties"] = r.get('n2').properties; 
                  // console.log(temp)

                  tempLink = {};
                  tempLink['id'] = r.get('r1').identity.low; 
                  tempLink['source'] = r.get('r1').start.low;
                  tempLink['target'] = r.get('r1').end.low;
                  tempLink['type'] = r.get('r1').type;
                  tempLink['properties'] = r.get('r1').properties;

                  if(!nodeSet.has(temp1['id'])){
                    nodeSet.add(temp1['id']);
                    nodeList.push(temp1);
                  }

                  if(!nodeSet.has(temp2['id'])){
                    nodeSet.add(temp2['id']);
                    nodeList.push(temp2);
                  }

                  linkList.push(tempLink);


                  // console.log(r.keys, r.keys.map(k => JSON.stringify(r.get(k))))
                }
             )

            let mp = {}; 
            mp['nodes'] = nodeList;
            mp['links'] = linkList; 

            res.send(mp,200);
  })
  .catch(error => {
    res.send(null,500);
    throw error;
  })
  .finally(() => {
    // return session.close();
  });

});


router.get('/graph', function(req, res, next) {
  session.run(
   `MATCH (n1)-[r1]->(n2)`
  )
  .then(result => {
    return result.records.map(record => {
      let temp = JSON.stringify(record);
      // console.log(temp);
      res.send(temp);
      // console.log(record.get(0))
    });
  })
  .catch(error => {
    // res.send(null,500);
    throw error;
  })
  .finally(() => {
    return session.close();
  });

  // res.send(null,200);
});



module.exports = router;
