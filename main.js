const neo4j = require('neo4j-driver')


const uri = 'bolt://localhost';
const user = 'neo4j'; 
const password = 'neo4j';

const driver = neo4j.driver(uri, neo4j.auth.basic(user, password))
const session = driver.session()
const personName = 'Alice'

  session.run(
    `MATCH (n1)-[r1]->(n2)
    RETURN n1,n2,r1
    `
  )
  .then(result => {
    // return result.records.map(record => {
    //   console.log(JSON.stringify(record));
    //   // console.log(record.get(0))
    // });
          // console.log(JSON.stringify(result.records));
    
            let nodeSet = new Set(); 
            let nodeList = [];
            let linkList = [];

            result.records.forEach(
            r => 
                {
                  temp1 = {}; 
                  temp1["id"] = r.get('n1').identity.low;
                  temp1["labels"] =  r.get('n1').labels;
                  temp1["properties"] = r.get('n1').properties; 

                  temp2 = {}; 
                  temp2["id"] = r.get('n2').identity.low;
                  temp2["labels"] =  r.get('n2').labels;
                  temp2["properties"] = r.get('n2').properties; 
                  // console.log(temp)

                  tempLink = {};
                  tempLink['id'] = r.get('r1').identity.low; 
                  tempLink['source'] = r.get('r1').start.low;
                  tempLink['target'] = r.get('r1').end.low;
                  tempLink['type'] = r.get('r1').type;
                  tempLink['properties'] = r.get('r1').properties;

                  if(!nodeSet.has(temp1['id'])){
                    nodeSet.add(temp1['id']);
                    nodeList.push(temp1);
                  }

                  if(!nodeSet.has(temp2['id'])){
                    nodeSet.add(temp2['id']);
                    nodeList.push(temp2);
                  }

                  linkList.push(tempLink);

                  // console.log(r.keys, r.keys.map(k => JSON.stringify(r.get(k))))
                }
             )

            console.log(nodeList);
            console.log(linkList);
  })
  .catch(error => {
    throw error;
  })
  .finally(() => {
    return session.close();
  });

// on application exit